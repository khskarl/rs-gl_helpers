use gl;
use gl::types::*;

use super::{Attachment, GLTexture};

#[derive(Debug, Hash)]
pub struct GLFramebuffer {
    id: GLuint,
}

impl GLFramebuffer {
    #[inline(always)]
    pub fn new(gl_texture: &GLTexture, buffers: &[Attachment], level: isize) -> Self {
        let framebuffer = GLFramebuffer {
            id: {
                let mut id = 0;
                unsafe {
                    gl::GenFramebuffers(1, &mut id);
                }
                id
            },
        };

        framebuffer.set(gl_texture, buffers, level);
        framebuffer
    }

    #[inline(always)]
    pub fn id(&self) -> GLuint {
        self.id
    }

    #[inline]
    pub fn bind(&self) -> &Self {
        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, self.id);
        }
        self
    }
    #[inline]
    pub fn unbind(&self) -> &Self {
        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
        }
        self
    }

    #[inline]
    pub fn set(&self, gl_texture: &GLTexture, buffers: &[Attachment], level: isize) {
        let gl_texture_id = gl_texture.id();

        let mut gl_enums = Vec::with_capacity(buffers.len());
        for i in 0..buffers.len() {
            gl_enums.push(buffers[i].into());
        }

        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, self.id);
            gl::BindTexture(gl_texture.kind().into(), gl_texture_id);

            for i in 0..gl_enums.len() {
                gl::FramebufferTexture2D(
                    gl::FRAMEBUFFER,
                    gl_enums[i],
                    gl::TEXTURE_2D,
                    gl_texture_id,
                    level as GLint,
                );
            }
            gl::DrawBuffers(buffers.len() as GLint, gl_enums.as_ptr());

            match gl::CheckFramebufferStatus(gl::FRAMEBUFFER) {
                gl::FRAMEBUFFER_UNDEFINED => panic!("Check framebuffer status failed undefined"),
                gl::FRAMEBUFFER_INCOMPLETE_ATTACHMENT => {
                    panic!("Check framebuffer status failed incomplete attachment")
                }
                gl::FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT => {
                    panic!("Check framebuffer status failed incomplete missing attachment")
                }
                gl::FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER => {
                    panic!("Check framebuffer status failed incomplete draw buffer")
                }
                gl::FRAMEBUFFER_INCOMPLETE_READ_BUFFER => {
                    panic!("Check framebuffer status failed incomplete read buffer")
                }
                gl::FRAMEBUFFER_UNSUPPORTED => {
                    panic!("Check framebuffer status failed unsupported")
                }
                gl::FRAMEBUFFER_INCOMPLETE_MULTISAMPLE => {
                    panic!("Check framebuffer status failed incomplete multisample")
                }
                gl::FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS => {
                    panic!("Check framebuffer status failed incomplete layer targets")
                }
                // gl::FRAMEBUFFER_COMPLETE => (),
                _ => (),
            }
        }
    }
}

impl Drop for GLFramebuffer {
    #[inline]
    fn drop(&mut self) {
        if self.id != 0 {
            unsafe {
                gl::DeleteFramebuffers(1, &self.id);
            }
        }
    }
}
