#![feature(link_args)]

extern crate gl;
extern crate gl_helpers;
extern crate glutin;
extern crate main_loop;

use gl::types::*;
use glutin::{Api, GlContext, GlRequest};

use gl_helpers::*;

#[cfg(target_os = "emscripten")]
#[allow(unused_attributes)]
#[link_args = "-s OFFSCREENCANVAS_SUPPORT=1"]
extern "C" {}

static SIMPLE_VERTEX_DATA: [GLfloat; 16] = [
    //   position     uv
    1f32, 1f32, 1f32, 1f32, -1f32, 1f32, 0f32, 1f32, 1f32, -1f32, 1f32, 0f32, -1f32, -1f32, 0f32,
    0f32,
];

static SIMPLE_VERTEX: &'static str = "
    #version 330

    uniform vec2 size;

    layout (location = 0) in vec2 position;
    layout (location = 1) in vec2 uv;

    out vec2 v_uv;

    void main() {
        gl_Position = vec4(position * size * 0.5, 0, 1.0);
        v_uv = uv;
    }
";

static SIMPLE_FRAGMENT: &'static str = "
    #version 330

    uniform float time;

    in vec2 v_uv;

    out vec4 out_color;

    void main() {
        out_color = vec4(v_uv, sin(time), 1.0);
    }
";

fn main() {
    let mut screen_width = 1024_usize;
    let mut screen_height = 768_usize;

    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new()
        .with_title("Simple")
        .with_maximized(true);
    let context = {
        let builder = glutin::ContextBuilder::new().with_vsync(true);

        builder
    };
    let gl_window = glutin::GlWindow::new(window, context, &events_loop).unwrap();

    unsafe {
        gl_window.make_current().unwrap();
        gl::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
    }

    let gl_info = GLInfo::new();
    println!("{}", gl_info.version());
    println!(
        "OpenGL version: {:?}.{:?}, GLSL version {:?}.{:?}0",
        gl_info.major(),
        gl_info.minor(),
        gl_info.glsl_major(),
        gl_info.glsl_minor()
    );

    gl_set_defaults();

    if let Some(logical_size) = gl_window.get_inner_size() {
        let physical_size = logical_size.to_physical(gl_window.get_hidpi_factor());
        screen_width = physical_size.width as usize;
        screen_height = physical_size.height as usize;
        gl_window.resize(physical_size);
        gl_set_viewport(0, 0, screen_width, screen_height);
    }

    let program = GLProgram::new(SIMPLE_VERTEX, SIMPLE_FRAGMENT);

    let buffer = GLBuffer::new(
        BufferTarget::Array,
        4,
        Usage::StaticDraw,
        &SIMPLE_VERTEX_DATA,
    );

    let mut vertex_array = GLVertexArray::new();

    vertex_array.bind();
    vertex_array.add_attribute(&buffer, program.get_attribute("position"), 0);
    vertex_array.add_attribute(&buffer, program.get_attribute("uv"), 2);

    vertex_array.enable_attributes();

    program.get_uniform("time").set_1f(1.0);

    let mut size = [1f32; 2];

    main_loop::glutin::run(&mut events_loop, move |events, ms_f64| {
        let ms = ms_f64 as f32;

        for event in events {
            match event {
                glutin::Event::WindowEvent { event, .. } => match event {
                    glutin::WindowEvent::CloseRequested => return main_loop::ControlFlow::Break,
                    _ => (),
                },
                _ => (),
            }
        }

        gl_set_clear_color(&[0.1, 0.1, 0.1, 1.0]);
        gl_clear(true, true, true);

        size[0] = 1.0 + ((ms * 0.005).cos() * 0.5);
        size[1] = 1.0 + ((ms * 0.005).sin() * 0.5);
        program.get_uniform("size").set_2f(&size);

        gl_draw_arrays(DrawMode::TriangleStrip, 0, 4);

        gl_window.swap_buffers().unwrap();

        main_loop::ControlFlow::Continue
    });
}
